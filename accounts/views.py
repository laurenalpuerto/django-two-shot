from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from accounts.forms import LoginForm

# Create your views here.


def user_login(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
    
            user = authenticate(
                request, 
                username=username, 
                password=password,
                )
    if user is not None:
                    login(request, user)
                    # If the person successfully logs in, redirect the browser to the URL registration named "home"
                    return redirect("home")
                    ...
    else:
        form = LoginForm()
    context = {
        "form": form,
    }

    return render(request, "account/login.html", context)

# Feature 9: Create a function in accounts/views.py that logs a person out then redirects them to the URL path registration named "login"
# In the accounts/urls.py, register that view in your urlpatterns list with the path "logout/" and the name "logout".

def user_logout(request):
    logout(request)
    return redirect("home")

# Feature 10 signup #2.

def user_signup(request):
    if request.method == "POST":
        form = SignupForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            # confirmation
            confirm = form.cleaned_data['password_confirmation']
            if password == confirm: 
                # You'll need to use the special create_user  method to create a new user account from their username and password.
                username=User.objects.create_user(username, password=password), 
                login(request, user) # login function that logs an acc in
                return redirect("home")
            else:
                form.add_error("password", "the passwords do not match") # 3. Feature 10
    else:
        form = SignUpForm()
    context = {
        "form": form,
    }

    return render(request, "account/signup.html", context)