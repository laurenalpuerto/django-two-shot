from django.forms import ModelForm
from receipts.models import Receipt

class LoginForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(
        max_length = 150,
        widget = forms.PasswordInput,
    )

# Feature 10 signup
class SignupForm(forms.Form):
    username = forms.Charfield(max_length=150)
    password = forms.CharField(
        max_length = 150,
        widget = forms.PasswordInput,
    )
    password_confirmation = forms.CharField(
        max_length = 150,
        widget = forms.PasswordInput,
    )

# Feature 11 Create Receipt View - may use a ModelForm

class ReceiptForm(forms.Form):
    class Meta:
        model = Receipt
        fields = (
            "vendor",
            "total",
            "tax",
            "date",
            "category",
            "account",
        )