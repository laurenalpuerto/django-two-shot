from django.shortcuts import render
from receipts.models import Receipt
from django.contrib.auth.decorators import login_required

# Create your views here.

#Feature 11 Create Receipt View
# May use a ModelForm (in forms.py)
# Register that view for the path "create/" in the receipts urls.py and the name "create_receipt".

@login_required     # Person must be logged in to see the view
def create_receipt(request):
        if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            # On a successful creation of a receipt, redirect the browser to "home"
            return redirect("home")
                    
        else:
            form = ReceiptForm()
        context = {
            "form": form,
        }

        return render(request, "receipt/create.html", context)

@login_required
def receipt_list(request):
    context = Receipt.objects.filter(purchaser=request.user)
    receipt = Receipts.objects.all()
    context = {
        "receipt_list": receipt,
    }

    return render(request, "receipts/list.html", context)

