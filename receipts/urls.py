from django.urls import path
from receipts.views import create_receipt, receipt_list

urlpatterns = [
    path("", receipt_list, name="receipt_list"),
    path("create/", create_receipt, name="create_receipt"),
    path("", receipt_list, name="home_page"),
]